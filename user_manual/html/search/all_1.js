var searchData=
[
  ['bo_5fcoh_7',['bo_coh',['../namespacevariables.html#afb6f8c4aeabffd06a2f747ba356ee4ca',1,'variables']]],
  ['bo_5fpop_8',['bo_pop',['../namespacevariables.html#a2a73e8becd0149734da585f50a1d47bb',1,'variables']]],
  ['bo_5fpop_5fsh_9',['bo_pop_sh',['../namespacevariables.html#a01a9d3567cc98f502a8e010d52c19297',1,'variables']]],
  ['boenergy_10',['boenergy',['../namespacevariables.html#a767fd537b71696ddaa20d6e81d503756',1,'variables']]],
  ['boforce_11',['boforce',['../namespacevariables.html#a2545c7aae8865e43afa2222be48b3762',1,'variables']]],
  ['boproblem_12',['boproblem',['../namespaceelectronic__problem.html#a15695c174f3d1f57d393e0fa7511e475',1,'electronic_problem']]]
];
